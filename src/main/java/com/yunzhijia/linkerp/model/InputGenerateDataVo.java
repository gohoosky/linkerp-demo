package com.yunzhijia.linkerp.model;

public class InputGenerateDataVo {
    private String eid;
    private String tableInfoId;
    private String actionId;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getTableInfoId() {
        return tableInfoId;
    }

    public void setTableInfoId(String tableInfoId) {
        this.tableInfoId = tableInfoId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    @Override
    public String toString() {
        return "InputGenerateDataVo [eid=" + eid + ", tableInfoId=" + tableInfoId + ", actionId=" + actionId + "]";
    }

}
