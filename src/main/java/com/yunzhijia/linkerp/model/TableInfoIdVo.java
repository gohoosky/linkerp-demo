package com.yunzhijia.linkerp.model;

public class TableInfoIdVo {
    private String eid;
    private String tableInfoId;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getTableInfoId() {
        return tableInfoId;
    }

    public void setTableInfoId(String tableInfoId) {
        this.tableInfoId = tableInfoId;
    }

    @Override
    public String toString() {
        return "TableInfoIdVo [eid=" + eid + ", tableInfoId=" + tableInfoId + "]";
    }
}
