package com.yunzhijia.linkerp.service;

import java.util.List;

import com.yunzhijia.linkerp.model.CategoryWithReportTableVo;
import com.yunzhijia.linkerp.model.DataSaveStatus;
import com.yunzhijia.linkerp.model.InputGenerateDataVo;
import com.yunzhijia.linkerp.model.BatchVo;
import com.yunzhijia.linkerp.model.TableInfoDataVo;
import com.yunzhijia.linkerp.model.TableInfoHeaderVo;
import com.yunzhijia.linkerp.model.TableInfoIdParam;
import com.yunzhijia.linkerp.model.TableInfoIdVo;

/**
 * 开放接口服务
 * 
 * @author cunshan_lu
 * @date 2017年10月13日 下午1:55:43
 */
public interface OpenApiService {
    /**
     * 获取轻应用管理员权限内所有报表分类及OpenApi创建的数据表信息
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午1:56:05
     * @param eid
     * @param accessToken
     * @return
     */
    List<CategoryWithReportTableVo> getAdminTableInfos(String eid, String accessToken);

    /**
     * 创建数据表头
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午1:56:34
     * @param header
     * @param accessToken
     * @return
     */
    TableInfoIdParam createReportTableInfoHeader(TableInfoHeaderVo header, String accessToken);

    /**
     * 上传数据表数据
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午1:56:46
     * @param tableData
     * @param accessToken
     * @return
     */
    boolean saveReportTableInfoData(TableInfoDataVo tableData, String accessToken);

    /**
     * 查询上传数据表数据上传结果
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午1:56:58
     * @param batchVo
     * @param accessToken
     * @return
     */
    List<DataSaveStatus> getReportTableInfoDataSaveStatus(BatchVo batchVo, String accessToken);

    /**
     * 生成图表数据
     * 
     * @author cunshan_lu
     * @date 2017年11月3日 上午11:24:54
     * @param tableIdData
     * @param accessToken
     * @return
     */
    boolean generateDiagramData(InputGenerateDataVo generateData, String accessToken);

    /**
     * 清空数据表数据
     * 
     * @author cunshan_lu
     * @date 2017年11月3日 上午11:24:41
     * @param tableIdData
     * @param accessToken
     * @return
     */
    boolean clearTableInfoData(TableInfoIdVo tableIdData, String accessToken);

}
