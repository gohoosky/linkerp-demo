package com.yunzhijia.linkerp;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.yunzhijia.linkerp.model.AccessToken;
import com.yunzhijia.linkerp.model.CategoryWithReportTableVo;
import com.yunzhijia.linkerp.model.DataSaveStatus;
import com.yunzhijia.linkerp.model.BatchVo;
import com.yunzhijia.linkerp.model.HeaderItemVo;
import com.yunzhijia.linkerp.model.InputGenerateDataVo;
import com.yunzhijia.linkerp.model.TableInfoDataVo;
import com.yunzhijia.linkerp.model.TableInfoHeaderVo;
import com.yunzhijia.linkerp.model.TableInfoIdParam;
import com.yunzhijia.linkerp.model.TableInfoIdVo;
import com.yunzhijia.linkerp.service.AccessTokenService;
import com.yunzhijia.linkerp.service.OpenApiService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTest {

    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private OpenApiService openApiService;

    /**
     * 获取令牌
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:29:41
     */
    @Test
    public void testGetAccessToken() {
        AccessToken accessToken = accessTokenService.getAccessToken();
        System.out.println(accessToken);
    }

    /**
     * 刷新令牌
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:30:01
     */
    @Test
    public void testrefreshToken() {
        AccessToken accessToken = accessTokenService.refreshToken();
        System.out.println(accessToken);
    }

    /**
     * 获取轻应用管理员权限内所有报表分类及OpenApi创建的数据表信息
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:30:17
     */
    @Test
    public void testGetAdminTableInfos() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        String eid = "13373723";// 需要修改为实际工作圈eid

        List<CategoryWithReportTableVo> categoryWithReportTableVos = openApiService.getAdminTableInfos(eid, accessToken.getAccessToken());
        System.out.println(categoryWithReportTableVos);
    }

    /**
     * 创建数据表头
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:32:08
     */
    @Test
    public void testCreateReportTableInfoHeader() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        TableInfoHeaderVo header = new TableInfoHeaderVo();
        header.setEid("13373723");// 需要修改为实际工作圈eid
        header.setName("自定义API测试123");
        header.setReportCategoryId("5ad6f3b8de02f6608061e06b"); // 报表分类Id,需要修改为AdminTableInfos获取的报表分类ID

        List<HeaderItemVo> headerItems = new ArrayList<HeaderItemVo>(); // 数据表头第一个字段类型必须为string且名称为recordId
        headerItems.add(new HeaderItemVo("recordId", "string"));
        headerItems.add(new HeaderItemVo("表头1", "string"));
        headerItems.add(new HeaderItemVo("表头2", "number"));
        headerItems.add(new HeaderItemVo("表头3", "timestamp"));
        header.setHeaderItems(headerItems);

        TableInfoIdParam tableInfoIdParam = openApiService.createReportTableInfoHeader(header, accessToken.getAccessToken());
        System.out.println(tableInfoIdParam);
    }

    /**
     * 上传数据表数据
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:32:31
     */
    @Test
    public void testSaveReportTableInfoData() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        TableInfoDataVo tableData = new TableInfoDataVo();
        // recordId为数据编号为必录项且必须唯一，如果上传数据与原有数据重复，执行更新操作
        String testData = "[{\"recordId\":\"600000000\",\"表头1\":\"中国\",\"表头2\":\"100\",\"表头3\":\"2008-08-08 20:00:00\"},{\"recordId\":\"600000001\",\"表头1\":\"美国\",\"表头2\":\"80\",\"表头3\":\"2009-09-08 20:00:00\"},{\"recordId\":\"600000003\",\"表头1\":\"法国\",\"表头2\":\"115\",\"表头3\":\"2008-08-08 20:00:00\"}]";
        tableData.setEid("13373723");// 需要修改为实际工作圈eid
        tableData.setReportCategoryId("5ad6f3b8de02f6608061e06b"); // 来源于AdminTableInfos,与表头保持一致
        tableData.setTableInfoId("5af42aa98704765e0a265d28");// 来源于表头创建或已有数据表Id
        tableData.setData(testData);
        tableData.setBatchId("abdayrsu1qaqZcZRLpwzsXvjbDX216");// 批次id,每次传不重复随机值，建议使用uuid
        tableData.setActionId("ZRLpwzsXvjbDX216"); //操作Id。每个操作Id对应多个批次
        boolean status = openApiService.saveReportTableInfoData(tableData, accessToken.getAccessToken());

        System.out.println(status);
    }

    /**
     * 查询上传数据表数据上传结果
     * 
     * @author cunshan_lu
     * @date 2017年10月13日 下午2:32:46
     */
    @Test
    public void testGetReportTableInfoDataSaveStatus() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        BatchVo batchVo = new BatchVo();
        batchVo.setEid("13373723");// 需要修改为实际工作圈eid
        batchVo.setBatchIds("abdayrsuqqaqZcZRLpwzsXvjbDX216"); // 批次id,与SaveReportTableInfoData批次id相同。支持批量上传以","分隔

        List<DataSaveStatus> statusList = openApiService.getReportTableInfoDataSaveStatus(batchVo, accessToken.getAccessToken());

        System.out.println(statusList);
    }

    /**
     * 生成图表数据
     * 
     * @author cunshan_lu
     * @date 2017年11月3日 下午3:23:32
     */
    @Test
    public void testGenerateDiagramData() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        InputGenerateDataVo generateData = new InputGenerateDataVo();
        generateData.setEid("13373723");// 工作圈eid
        generateData.setTableInfoId("5af42aa98704765e0a265d28");// 已有数据表Id
        generateData.setActionId("ZRLpwzsXvjbDX216"); //操作Id。
        boolean status = openApiService.generateDiagramData(generateData, accessToken.getAccessToken());

        System.out.println(status);
    }

    /**
     * 清空数据表数据
     * 
     * @author cunshan_lu
     * @date 2017年11月3日 下午3:26:17
     */
    @Test
    public void testClearTableInfoData() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        TableInfoIdVo tableIdData = new TableInfoIdVo();
        tableIdData.setEid("13373723");// 工作圈eid
        tableIdData.setTableInfoId("5a9905378704761067033055");// 已有数据表Id

        boolean status = openApiService.clearTableInfoData(tableIdData, accessToken.getAccessToken());

        System.out.println(status);
    }

}
